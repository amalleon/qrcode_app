package com.applet.qr_code;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class checknetwork {

    Context context;
    public checknetwork(Context ctx){
        context = ctx;
    }

    public boolean checknetworkbool()
    {
        ConnectivityManager conn = (ConnectivityManager)
                context.getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfos = conn.getActiveNetworkInfo();

        if(networkInfos != null)
        {
            if(networkInfos.getType() == ConnectivityManager.TYPE_WIFI)
            {
                return true;
            }
            else if(networkInfos.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                return true;
            }
        }
        else
        {
            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("Network Status");
            build.setMessage("Not connected");
            build.setCancelable(false);
            build.setPositiveButton("Connect",new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
            build.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        return false;
    }
}
