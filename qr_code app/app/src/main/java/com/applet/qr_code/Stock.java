package com.applet.qr_code;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.app.ActionBar.LayoutParams;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Stock#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Stock extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Spinner spinner;
    ArrayAdapter adaptar;
    TextView search_date;

    public static TableLayout table;
    public static TextView prouid,prosize,proordnum,prodate,bremain,nremain,gremain,bused,nused,gused;

    public Stock() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Stock.
     */
    // TODO: Rename and change types and number of parameters
    public static Stock newInstance(String param1, String param2) {
        Stock fragment = new Stock();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_stock, container, false);

        spinner = view.findViewById(R.id.spinner3);
        String[] size = {"All Size","3ply","4ply","6ply","8ply","9ply","12ply","16ply"};
        adaptar = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,size);
        adaptar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptar);

        TextView text = view.findViewById(R.id.allstock);
        text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    allstocks();
                }
            });

        table = view.findViewById(R.id.table2);
        prouid = view.findViewById(R.id.stockuid);
        prosize = view.findViewById(R.id.stocksize);
        proordnum = view.findViewById(R.id.stocknum);
        prodate = view.findViewById(R.id.stockdate);
        bremain = view.findViewById(R.id.srbags);
        nremain = view.findViewById(R.id.srnetwt);
        gremain = view.findViewById(R.id.srgrosswt);
        bused = view.findViewById(R.id.subags);
        nused = view.findViewById(R.id.sunetwt);
        gused = view.findViewById(R.id.sugrosswt);
        search_date = view.findViewById(R.id.stock_search_date);

        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        search_date.setText(date);

        ImageView stock_search_date = view.findViewById(R.id.imageView);
        stock_search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_details.search_key = 1;
                calandercall();
            }
        });

        ImageView stocklist_search_date = view.findViewById(R.id.stocklistimageView);
        stocklist_search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_details.search_key = 2;
                calandercall();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, View view, int i, long l) {

                int count = table.getChildCount();
                table.removeViews(1, count-1);

                currentstock currentstock = new currentstock(getContext());
                currentstock.execute(search_date.getText().toString(),spinner.getSelectedItem().toString());

                return;
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getActivity().getApplicationContext(),"nothing",Toast.LENGTH_SHORT).show();
                return;
            }
        });

        /*currentproduction currentproduction = new currentproduction(getContext());
        currentproduction.execute(spinner.getSelectedItem().toString()); */

       /* int i;
        for(i=0;i<10;i++) {

            TableRow row = new TableRow(getContext());
            row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 152));

            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Monospace.ttf");

            TextView sno = new TextView(getContext());
            sno.setText("4");
            sno.setTextSize(18);
            sno.setTextColor(Color.BLACK);
            sno.setHeight(150);
            sno.setWidth(70);
            sno.setTypeface(typeface);
            sno.setGravity(Gravity.CENTER);
            row.addView(sno);

            TextView label_hello = new TextView(getContext());
            label_hello.setText("12/12/2020");
            label_hello.setTextSize(18);
            label_hello.setTextColor(Color.BLACK);
            label_hello.setHeight(150);
            //label_hello.setWidth(300);
            label_hello.setTypeface(typeface);
            label_hello.setGravity(Gravity.CENTER);
            row.addView(label_hello);

            TextView label_android = new TextView(getContext());
            label_android.setText("Customer");
            label_android.setTextSize(18);
            label_android.setTextColor(Color.BLACK);
            label_android.setHeight(150);
            label_android.setTypeface(typeface);
            label_android.setGravity(Gravity.CENTER);
            row.addView(label_android);

            table.addView(row);
        } */

        return view;
    }
    public void allstocks()
    {
        Intent intent = new Intent(getContext(),allstock.class);
        startActivity(intent);
    }
    public void calandercall()
    {
        datepicker datepicker = new datepicker();
        datepicker.show(getFragmentManager(),"Date Picker");
    }
}