package com.applet.qr_code;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class sessionmanage extends Activity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String SHARED_PREF_NAME = "session";
    String SESSION_KEY = "session_id";

    public sessionmanage(Context context)
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

        public void saveSession()
        {
            //save session of user whenever user is logged in
            int id = 2001110120;
            editor.putInt(SESSION_KEY,id).commit();
        }
        public void flagsession()
        {
            editor.putBoolean("flag",true).commit();
        }

        public boolean getflagSession()
        {
            return sharedPreferences.getBoolean("flag", false);
        }

        public int getSession(){
            //return user id whose session is saved
            return sharedPreferences.getInt(SESSION_KEY, -1);
        }

        public void removeSession(){
            editor.putInt(SESSION_KEY,-1).commit();
        }

        public void flagremoveSession(){
        editor.putBoolean("flag",false).commit();
    }

}
