package com.applet.qr_code;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.TableRow;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

public class currentproduction extends AsyncTask<String,Void,String> {

    Context context;
    production production;

    public currentproduction(Context ctx) {
        context = ctx;
    }

    @Override
    protected String doInBackground(String... strings) {

        String date = strings[0];
        String dsize = strings[1];
        //String link = "http://192.168.43.64/revenge/HTML5Application4/public_html/qr_code/appphp/currentproduction.php";
        urls urls = new urls();
        String link = urls.currentproduction();
        URL url = null;
        try {
            url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(1000);
            httpURLConnection.setReadTimeout(1000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();

            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data = URLEncoder.encode("date","UTF-8")+"="+URLEncoder.encode(date,"UTF-8")+"&"
                              +URLEncoder.encode("size","UTF-8")+"="+URLEncoder.encode(dsize,"UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            int response_code = httpURLConnection.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK)
            {
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                StringBuilder result = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                    break;
                }
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            } else {
                return "unsuccessful";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        production = new production();
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
        {
            production.prouid.setText("");
            production.prosize.setText("");
            production.proordnum.setText("");
            production.prodate.setText("");
            production.purchasecustomer.setText("");
            production.bremain.setText("");
            production.nremain.setText("");
            production.gremain.setText("");
            production.bused.setText("");
            production.nused.setText("");
            production.gused.setText("");

            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("Purchase Status");
            build.setMessage("Server not connected...");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else if (result.equalsIgnoreCase("false"))
        {
            production.prouid.setText("");
            production.prosize.setText("");
            production.proordnum.setText("");
            production.prodate.setText("");
            production.purchasecustomer.setText("");
            production.bremain.setText("");
            production.nremain.setText("");
            production.gremain.setText("");
            production.bused.setText("");
            production.nused.setText("");
            production.gused.setText("");

            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("Purchase Status");
            build.setMessage("Not in database...");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else {
         
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(result);
                    String[] protable = new String[jsonArray.length()];

                    JSONObject obj = jsonArray.getJSONObject(0);
                    production.prouid.setText(obj.getString("uniqueid"));
                    production.prosize.setText(obj.getString("size"));
                    production.proordnum.setText(obj.getString("purchasenumber"));
                    production.prodate.setText(obj.getString("purchasedate"));
                    production.purchasecustomer.setText(obj.getString("customer"));

                    int totalbags = Integer.parseInt(obj.getString("bags"));
                    int totalnetwt = Integer.parseInt(obj.getString("totalnetweight"));
                    int totalgrosswt = Integer.parseInt(obj.getString("totalgrossweight"));

                    int i,bags = 0,netwt=0,grosswt=0;
                   for (i = 1; i < jsonArray.length(); i++)
                   {
                        JSONObject jobj = jsonArray.getJSONObject(i);
                        addtable(jobj.getString("boxnumber"),jobj.getString("box_date"),
                                jobj.getString("netweight"),jobj.getString("grossweight"),jobj.getString("size"),
                                jobj.getString("scanneddate"));
                        if(!jobj.getString("scanneddate").equalsIgnoreCase(""))
                        {
                            bags++; netwt += Integer.parseInt(jobj.getString("netweight"));
                            grosswt += Integer.parseInt(jobj.getString("grossweight"));
                        }
                    }

                   production.bremain.setText(String.valueOf(totalbags-bags));
                   production.nremain.setText(String.valueOf(totalnetwt-netwt));
                   production.gremain.setText(String.valueOf(totalgrosswt-grosswt));
                   production.bused.setText(String.valueOf(bags));
                   production.nused.setText(String.valueOf(netwt));
                   production.gused.setText(String.valueOf(grosswt));

                } catch (JSONException e) {
                    e.printStackTrace();
            }
    }
}

    public void addtable(String Boxno,String Date,String netwt,String grosswt,String size,String sdate)
    {
        TableRow row = new TableRow(context);
        row.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, 152));

        //Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Monospace.ttf");

        TextView boxno = new TextView(context);
        boxno.setText(Boxno);
        boxno.setTextSize(10);
        boxno.setTextColor(Color.parseColor("#5e5e5e"));
        boxno.setHeight(150);
        boxno.setWidth(59);
       // boxno.setTypeface(typeface);
        boxno.setGravity(Gravity.CENTER);
        boxno.setBackgroundResource(R.drawable.border);
        row.addView(boxno);

        TextView date = new TextView(context);
        date.setText(Date);
        date.setTextSize(10);
        date.setTextColor(Color.parseColor("#5e5e5e"));
        date.setHeight(150);
        date.setWidth(69);
     //   date.setTypeface(typeface);
        date.setGravity(Gravity.CENTER);
        date.setBackgroundResource(R.drawable.border);
        row.addView(date);

        TextView ntwt = new TextView(context);
        ntwt.setText(netwt);
        ntwt.setTextSize(10);
        ntwt.setTextColor(Color.parseColor("#5e5e5e"));
        ntwt.setHeight(150);
        ntwt.setWidth(59);
     //   ntwt.setTypeface(typeface);
        ntwt.setGravity(Gravity.CENTER);
        ntwt.setBackgroundResource(R.drawable.border);
        row.addView(ntwt);

        TextView grswt = new TextView(context);
        grswt.setText(grosswt);
        grswt.setTextSize(10);
        grswt.setTextColor(Color.parseColor("#5e5e5e"));
        grswt.setHeight(150);
        grswt.setWidth(59);
      //  grswt.setTypeface(typeface);
        grswt.setGravity(Gravity.CENTER);
        grswt.setBackgroundResource(R.drawable.border);
        row.addView(grswt);

        TextView siz = new TextView(context);
        siz.setText(size);
        siz.setTextSize(10);
        siz.setTextColor(Color.parseColor("#5e5e5e"));
        siz.setHeight(150);
        siz.setWidth(59);
       // siz.setTypeface(typeface);
        siz.setGravity(Gravity.CENTER);
        siz.setBackgroundResource(R.drawable.border);
        row.addView(siz);

        TextView scanndate = new TextView(context);
        scanndate.setText(sdate);
        scanndate.setTextSize(10);
        scanndate.setTextColor(Color.parseColor("#5e5e5e"));
        scanndate.setHeight(150);
        scanndate.setWidth(69);
      //  scanndate.setTypeface(typeface);
        scanndate.setGravity(Gravity.CENTER);
        scanndate.setBackgroundResource(R.drawable.border);
        row.addView(scanndate);

        production.table.addView(row);
    }
}