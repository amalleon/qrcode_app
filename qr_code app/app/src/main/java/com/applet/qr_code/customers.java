package com.applet.qr_code;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class customers extends AsyncTask<String,Void,String> {


    Context context;
    customers (Context ctx) {
        context = ctx;
    }
    @Override
    protected String doInBackground(String... strings) {

        //String link = "http://192.168.43.64/revenge/HTML5Application4/public_html/qr_code/appphp/customer.php";
        urls urls = new urls();
        String link = urls.customer_get();
        URL url = null;
        try {
            url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();

            int response_code = httpURLConnection.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                StringBuilder result = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                    break;
                }
                inputStream.close();
                httpURLConnection.disconnect();
                return result.toString();
            }
            else{
                return "unsuccessful";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
        {
            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("Customer Status");
            build.setMessage("Server not connected...");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(result);
                result robj = new result();
                dispatch_bags dispatch_bags = new dispatch_bags();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    robj.list.add(obj.getString("customername"));
                    dispatch_bags.customerlist.add(obj.getString("customername"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
