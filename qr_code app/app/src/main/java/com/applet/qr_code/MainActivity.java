package com.applet.qr_code;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Context context;
    CodeScanner codeScanner;
    CodeScannerView scannview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startscanning();
    }

    @Override
    protected void onStart() {
        super.onStart();
        sessionmanage sobj = new sessionmanage(this);
        sobj.flagremoveSession();
    }

    // scan start
    public void startscanning()
    {
        scannview = findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(this,scannview);

        codeScanner.setDecodeCallback(new DecodeCallback() {

                @Override
                public void onDecoded(@NonNull final Result result) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            openactivity(result.getText());
                        }
                    });
                }
            });
    }

    // after scann show result so calling result activity
    public void openactivity(String value)
    {
        Intent intent = new Intent(this,result.class);
        intent.putExtra("qrresult",value);
        startActivity(intent);
    }

    // start the camera
    @Override
    protected void onResume()
    {
        super.onResume();
        camerarequest();
    }

    @Override
    protected void onPause() {

        super.onPause();
        codeScanner.stopPreview();
        codeScanner.releaseResources();
    }

    // asking camera request if not
    public void camerarequest()
    {
        Dexter.withActivity(this).withPermission(Manifest.permission.CAMERA).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                codeScanner.startPreview();
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getApplicationContext(),"Access Denied",Toast.LENGTH_SHORT).show();
                finish();
                System.exit(0);
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    // logout or close the session and open login activity
    public void logout(View view)
    {
        AlertDialog.Builder build = new AlertDialog.Builder(this);
        build.setTitle("Logout Status");
        build.setMessage("Do you want to logout or not ?");
        build.setPositiveButton("Ok",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sessionmanage obj = new sessionmanage(MainActivity.this);
                obj.removeSession();
                Intent intent = new Intent(getApplicationContext(),login.class);
                startActivity(intent);
                finish();
            }
        });
        build.setNegativeButton("Cancel", null);
        AlertDialog alertDialog = build.create();
        alertDialog.show();

    }

    // search databse
    public void searchdatabase(View view)
    {
        checknetwork checknetwork = new checknetwork(this);
        if(checknetwork.checknetworkbool()) {
            codeScanner.stopPreview();
            Intent intent = new Intent(getApplicationContext(),view_details.class);
            startActivity(intent);
        }
    }
}