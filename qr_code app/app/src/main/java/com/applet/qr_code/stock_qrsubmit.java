package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class stock_qrsubmit extends AppCompatActivity {

    private LinearLayout parentLinearLayout;
    public static List<String> stock_items_list = new ArrayList<String>();
    public static List<String> stock_items_id_list = new ArrayList<>();
    private int i;
    private String bagno,dsize,dnetwt,dgrosswt;
    TextView boxno,size,netwt,grosswt;
    dispatch_bags dispatch_bags = new dispatch_bags();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_qrsubmit);

        parentLinearLayout=(LinearLayout) findViewById(R.id.parent_linear_layout);

        bagno = getIntent().getStringExtra("bagno");
        dsize = getIntent().getStringExtra("size");
        dnetwt = getIntent().getStringExtra("netwt");
        dgrosswt = getIntent().getStringExtra("grosswt");

        boxno = findViewById(R.id.stock_boxno);
        boxno.setText(bagno);

        size = findViewById(R.id.stock_size);
        size.setText(dsize);

        netwt = findViewById(R.id.stock_netwt);
        netwt.setText(dnetwt);

        grosswt = findViewById(R.id.stock_grosswt);
        grosswt.setText(dgrosswt);

        for(i=0;i<stock_items_list.size();i+=4) {
            LayoutInflater inflater = getLayoutInflater();
            View rowView=inflater.inflate(R.layout.stock_items, null,true);
            TextView id = (TextView) rowView.findViewById(R.id.stock_list_bagno);
            id.setText(stock_items_list.get(i));
            TextView id2 = (TextView) rowView.findViewById(R.id.stock_list_size);
            id2.setText(stock_items_list.get(i+1));
            TextView id3 = (TextView) rowView.findViewById(R.id.stock_list_netwt);
            id3.setText(stock_items_list.get(i+2));
            TextView id4 = (TextView) rowView.findViewById(R.id.stock_list_grosswt);
            id4.setText(stock_items_list.get(i+3));
            parentLinearLayout.addView(rowView, 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        dispatch_bags.tbags = 0;
        dispatch_bags.tnetwt = 0;
        dispatch_bags.tgrosswt = 0;

    }

    public void onAddField(View v) {

        // flag session for submit check
        sessionmanage session = new sessionmanage(this);
        if(session.getflagSession() == false) {
            if(stock_items_id_list.contains(bagno))
            {
                AlertDialog.Builder build = new AlertDialog.Builder(this);
                build.setTitle("Add bag Status");
                build.setMessage("Already added.");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
            }
            else
            {
                LayoutInflater inflater = getLayoutInflater();
                View rowView=inflater.inflate(R.layout.stock_items, null,true);

                stock_items_id_list.add(bagno);
                stock_items_list.add(bagno);
                stock_items_list.add(dsize);
                stock_items_list.add(dnetwt);
                stock_items_list.add(dgrosswt);

                TextView id = (TextView) rowView.findViewById(R.id.stock_list_bagno);
                id.setText(bagno);
                TextView id2 = (TextView) rowView.findViewById(R.id.stock_list_size);
                id2.setText(dsize);
                TextView id3 = (TextView) rowView.findViewById(R.id.stock_list_netwt);
                id3.setText(dnetwt);
                TextView id4 = (TextView) rowView.findViewById(R.id.stock_list_grosswt);
                id4.setText(dgrosswt);

                parentLinearLayout.addView(rowView, 0);
            }
        }
        else{
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Add bag Status");
            build.setMessage("Already added.");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
    }
    public void onDelete(View v) {
        parentLinearLayout.removeView((View) v.getParent());

        stock_items_list.clear();
        for(int c=0; c < parentLinearLayout.getChildCount(); c++) {
            View childView = parentLinearLayout.getChildAt(c);
            TextView id1 = (TextView) (childView.findViewById(R.id.stock_list_bagno));
            TextView id2 = (TextView)childView.findViewById(R.id.stock_list_size);
            TextView id3 = (TextView) childView.findViewById(R.id.stock_list_netwt);
            TextView id4 = (TextView) childView.findViewById(R.id.stock_list_grosswt);
            stock_items_id_list.add(id1.getText().toString());
            stock_items_list.add(id1.getText().toString());
            stock_items_list.add(id2.getText().toString());
            stock_items_list.add(id3.getText().toString());
            stock_items_list.add(id4.getText().toString());
        }
    }
    public void dispatch(View view)
    {
        String values = "";
        for(int c=0; c < parentLinearLayout.getChildCount(); c++) {
            View childView = parentLinearLayout.getChildAt(c);
            TextView id1 = (TextView) (childView.findViewById(R.id.stock_list_bagno));
            TextView id2 = (TextView) childView.findViewById(R.id.stock_list_netwt);
            TextView id3 = (TextView) childView.findViewById(R.id.stock_list_grosswt);
            values += id1.getText().toString()+",";
            dispatch_bags.tbags++;
            dispatch_bags.tnetwt += Integer.parseInt(id2.getText().toString());
            dispatch_bags.tgrosswt += Integer.parseInt(id3.getText().toString());
        }
        if(values == "")
        {
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Dispatch Status");
            build.setMessage("No Bags were added to dispatch.");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else{
            Intent intent = new Intent(this,dispatch_bags.class);
            intent.putExtra("bags",values);
            startActivity(intent);
        }
    }

    public void clearall(View v)
    {
        stock_items_id_list.clear();
        stock_items_list.clear();
        parentLinearLayout.removeAllViews();
    }

}