package com.applet.qr_code;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class customer_post extends AsyncTask<String,Void,String> {

        String name,key;
        Context context;
        customer_post (Context ctx) {
            context = ctx;
        }
        @Override
        protected String doInBackground(String... params) {
            name = params[0];
            key = params[1];
            //String link = "http://192.168.43.64/revenge/HTML5Application4/public_html/qr_code/appphp/customer_post.php";
            urls urls = new urls();
            String link = urls.customer_post();
            URL url = null;
            try {
                url = new URL(link);

                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setConnectTimeout(1000);
                httpURLConnection.setReadTimeout(1000);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();

                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                int response_code = httpURLConnection.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return result;
                }
                else{
                    return "unsuccessful";
                }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        }

    return null;
   }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
            {
                AlertDialog.Builder build = new AlertDialog.Builder(context);
                build.setTitle("Customer Status");
                build.setMessage("Server not connected...");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
                if(key.equalsIgnoreCase("1"))
                {
                    dispatch_bags obj = new dispatch_bags();
                    int pos = obj.customerlist.indexOf(name);
                    obj.customerlist.remove(pos);
                    obj.spinner.setSelection(0);
                }
                else
                {
                    result robj = new result();
                    int pos = robj.list.indexOf(name);
                    robj.list.remove(pos);
                    robj.spinner.setSelection(0);
                }

            }
            else if(result.equalsIgnoreCase("exist")) {
                AlertDialog.Builder build = new AlertDialog.Builder(context);
                build.setTitle("Customer Status");
                build.setMessage("Customer name already exist...");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
                if(key.equalsIgnoreCase("1"))
                {
                    dispatch_bags obj = new dispatch_bags();
                    int pos = obj.customerlist.indexOf(name);
                    obj.customerlist.remove(pos);
                    obj.spinner.setSelection(0);
                }
                else
                {
                    result robj = new result();
                    int pos = robj.list.indexOf(name);
                    robj.list.remove(pos);
                    //robj.spinner.setSelection(0);
                }
            }
            else if(result.equalsIgnoreCase("false")) {
                AlertDialog.Builder build = new AlertDialog.Builder(context);
                build.setTitle("Customer Status");
                build.setMessage("Customer name not submitted...");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
                if(key.equalsIgnoreCase("1"))
                {
                    dispatch_bags obj = new dispatch_bags();
                    int pos = obj.customerlist.indexOf(name);
                    obj.customerlist.remove(pos);
                    obj.spinner.setSelection(0);
                }
                else
                {
                    result robj = new result();
                    int pos = robj.list.indexOf(name);
                    robj.list.remove(pos);
                    //robj.spinner.setSelection(0);
                }
            }
        }

}
