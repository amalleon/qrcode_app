package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.Toast;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class login extends AppCompatActivity {

    ProgressDialog p;
    EditText username, password;
    String user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        password = findViewById(R.id.password);
        password.setHint("Password");
    }

    @Override
    protected void onStart() {
        super.onStart();
        sessionmanage obj = new sessionmanage(this);
        int id = obj.getSession();
        if (id != -1) {
            movetohome();
        }
    }

    // move to mainactivity
    public void movetohome() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    // login button click
    public void logincheck(View view) {

        username = findViewById(R.id.username);
        user = (username.getText()).toString();
        pass = (password.getText()).toString();

        checknetwork checknetwork = new checknetwork(this);
        if(checknetwork.checknetworkbool())
        {
            loginback log = new loginback(this);
            log.execute(user, pass);
        }
    }

    //   login check in database

    public class loginback extends AsyncTask<String, Void, String> {

        Context context;

        public loginback(Context ctx) {
            context = ctx;
        }

        @Override
        protected String doInBackground(String... strings) {

            //String login_url = "http://192.168.43.64/QR_code/appphp/login.php";
            //login_url = "http://inventory-scan.000webhostapp.com/login.php";
            urls urls = new urls();
            String link = urls.login();
            URL url = null;
            try {
                url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String post_data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(strings[0], "UTF-8") + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(strings[1], "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                int response_code = httpURLConnection.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK)
                {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String line = "";
                    StringBuilder result = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        result.append(line);
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return result.toString();
                }
                else{
                    return "unsuccessful";
                }
            }
                catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(context);
            p.setMessage("Please wait...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            p.hide();
            if (result.equalsIgnoreCase("true")) {
                sessionmanage obj = new sessionmanage(context);
                obj.saveSession();
                movetohome();
            }
            else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
            {
                AlertDialog.Builder build = new AlertDialog.Builder(context);
                build.setTitle("Login Status");
                build.setMessage("Server not connected...");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
            }
            else{
                AlertDialog.Builder build = new AlertDialog.Builder(context);
                build.setTitle("Login Status");
                build.setMessage("Wrong username or password...");
                AlertDialog alertDialog = build.create();
                alertDialog.show();
            }
        }
    }
}