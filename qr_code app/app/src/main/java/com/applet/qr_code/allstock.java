package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.os.Bundle;
import android.widget.ListView;
import android.app.Activity;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class allstock extends AppCompatActivity {

    public static ListView view;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allstock);

        allstockback allstockback = new allstockback(this);
        allstockback.execute();
    }

    public class MyListAdapter extends ArrayAdapter<String> {


        private final Activity context;
        private final String[] uid;
        private final String[] date;
        private final String[] size;
        private final String[] num;
        private final String[] bused;
        private final String[] bremain;
        private final String[] nused;
        private final String[] nremain;
        private final String[] gused;
        private final String[] gremain;
        private final String[] pcustomer;

        public MyListAdapter(Activity context, String[] uid, String[] date, String[] size, String[] num, String[] bused,
                             String[] bremain,String[] nused, String[] nremain,String[] gused, String[] gremain,String[] pcustomer) {
            super(context, R.layout.customlist, num);
            // TODO Auto-generated constructor stub

            this.context=context;
            this.uid=uid;
            this.num=num;
            this.date=date;
            this.size=size;
            this.bused=bused;
            this.bremain=bremain;
            this.nused=nused;
            this.nremain=nremain;
            this.gused=gused;
            this.gremain=gremain;
            this.pcustomer=pcustomer;
        }

        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.customlist, null,true);

            TextView id = (TextView) rowView.findViewById(R.id.uid);
            id.setText(uid[position]);
            TextView titleText = (TextView) rowView.findViewById(R.id.date);
            titleText.setText(date[position]);
            TextView titleText1 = (TextView) rowView.findViewById(R.id.size);
            titleText1.setText(size[position]);
            TextView titleText2 = (TextView) rowView.findViewById(R.id.number);
            titleText2.setText(num[position]);

            TextView titleText3 = (TextView) rowView.findViewById(R.id.bused);
            titleText3.setText(bused[position]);
            TextView titleText4 = (TextView) rowView.findViewById(R.id.bremain);
            titleText4.setText(bremain[position]);

            TextView titleText5 = (TextView) rowView.findViewById(R.id.nused);
            titleText5.setText(nused[position]);
            TextView titleText6 = (TextView) rowView.findViewById(R.id.nremain);
            titleText6.setText(nremain[position]);

            TextView titleText7 = (TextView) rowView.findViewById(R.id.gused);
            titleText7.setText(gused[position]);
            TextView titleText8 = (TextView) rowView.findViewById(R.id.gremain);
            titleText8.setText(gremain[position]);

            TextView titleText9 = (TextView) rowView.findViewById(R.id.no);
            titleText9.setText("Stock No");
            TextView titleText10 = (TextView) rowView.findViewById(R.id.uors);
            titleText10.setText("Sold");

            TextView titleText11 = (TextView) rowView.findViewById(R.id.pcustomer);
            titleText11.setText(pcustomer[position]);
            //titleText11.setVisibility(View.GONE);

            return rowView;

        };
    }

    public void goback(View view)
    {
        finish();
    }

    public class allstockback extends AsyncTask<String,Void,String>
    {

        Context context;
        public allstockback(Context ctx) {  context = ctx; }

        @Override
        protected String doInBackground (String...strings)
        {
            //String link = "http://192.168.43.64/revenge/HTML5Application4/public_html/qr_code/appphp/allstock.php";
            urls urls = new urls();
            String link = urls.allstock();
            URL url = null;
            try {
                url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setConnectTimeout(1000);
                httpURLConnection.setReadTimeout(1000);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();

                int response_code = httpURLConnection.getResponseCode();
                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    StringBuilder result = new StringBuilder();
                    String line = null;
                    while ((line = bufferedReader.readLine()) != null) {
                        result.append(line);
                        break;
                    }
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return result.toString();
                } else {
                    return "unsuccessful";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            }
            return null;
        }

        @Override
        protected void onPostExecute (String result){
        super.onPostExecute(result);
        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
        {
            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("All Production Status");
            build.setMessage("Server not connected...");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else {

            JSONArray jsonArray = null;
            try
            {
                jsonArray = new JSONArray(result);
                String[] uid = new  String[jsonArray.length()/2];
                String[] date = new  String[jsonArray.length()/2];
                String[] num = new  String[jsonArray.length()/2];
                String[] size = new  String[jsonArray.length()/2];
                String[] bused = new  String[jsonArray.length()/2];
                String[] bremain = new  String[jsonArray.length()/2];
                String[] nused = new  String[jsonArray.length()/2];
                String[] nremain = new  String[jsonArray.length()/2];
                String[] gused = new  String[jsonArray.length()/2];
                String[] gremain = new  String[jsonArray.length()/2];
                String[] pcustomer = new  String[jsonArray.length()/2];

                int j =0;
                for (int i = 1; i <= jsonArray.length(); i+=2)
                {
                    JSONObject jobj = jsonArray.getJSONObject(i-1);
                    uid[j] =  jobj.getString("uniqueid");
                    date[j] = jobj.getString("stockdate");
                    size[j] = jobj.getString("size");
                    num[j] = jobj.getString("stocknumber");
                    pcustomer[j] = jobj.getString("customer");
                    JSONObject obj = jsonArray.getJSONObject(i);
                    bused[j] = obj.getString("bused");
                    bremain[j] = obj.getString("bremain");
                    nused[j] = obj.getString("nused");
                    nremain[j] = obj.getString("nremain");
                    gused[j] = obj.getString("gused");
                    gremain[j] = obj.getString("gremain");
                    j++;
                }

                view  = findViewById(R.id.list);
                MyListAdapter adapter=new MyListAdapter((Activity) context,uid,date,size,num,bused,bremain,nused,nremain,gused,gremain,pcustomer);
                view.setAdapter(adapter);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    }
}