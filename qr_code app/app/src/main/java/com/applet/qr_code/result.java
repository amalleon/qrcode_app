package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.*;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class result extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    String data,m,d;
    public static List<String> list=new ArrayList<String>();
    public static Spinner spinner;
    TextView uniqueid,date,boxno,optional,customerlabel,type,size,netwt,grosswt,scandate;
    EditText customername;
    ArrayAdapter adaptar;
    private Context context;
    int i,j=0; String temp = ""; String[] finaldata = new String[9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        list.clear();
        list.add("CUSTOMER");
        list.add("New");

        data = getIntent().getStringExtra("qrresult");

        for(i=0;i<data.length();i++)
        {
            if(data.charAt(i) != ',')
            {
                temp += data.charAt(i);
            }
            else
            {
                finaldata[j] = temp;
                j++;
                temp = "";
            }
        }
        try {
            if (Integer.parseInt(finaldata[0]) == 111012001) {
                if (finaldata[3].equalsIgnoreCase("stock") && finaldata[1].equalsIgnoreCase(""))
                {
                    Intent intent = new Intent(this,stock_qrsubmit.class);
                    intent.putExtra("bagno",finaldata[2]);
                    intent.putExtra("size",finaldata[4]);
                    intent.putExtra("netwt",finaldata[6]);
                    intent.putExtra("grosswt",finaldata[7]);
                    startActivity(intent);
                    finish();
                }
                else {
                    uniqueid = findViewById(R.id.uniqueid);
                    uniqueid.setText(finaldata[1]);

                    boxno = findViewById(R.id.boxno);
                    boxno.setText(finaldata[2]);

                    type = findViewById(R.id.type);

                    size = findViewById(R.id.size);
                    size.setText(finaldata[4]);

                    date = findViewById(R.id.cdate);
                    date.setText(finaldata[5]);

                    netwt = findViewById(R.id.netwt);
                    netwt.setText(finaldata[6]);

                    grosswt = findViewById(R.id.grosswt);
                    grosswt.setText(finaldata[7]);

                    scandate = findViewById(R.id.scandate);
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                    scandate.setText(date);

                    optional = findViewById(R.id.optional);
                    customerlabel = findViewById(R.id.customerlabel);
                    spinner = findViewById(R.id.spinner);
                    if (finaldata[3].equalsIgnoreCase("stock")) {
                        type.setText("STOCK");
                        optional.setVisibility(View.VISIBLE);
                        customerlabel.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.VISIBLE);

                        customers cobj = new customers(this);
                        cobj.execute();
                        adaptar = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list);
                        adaptar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adaptar);
                    } else {
                        type.setText("PURCHASE");
                        optional.setVisibility(View.GONE);
                        customerlabel.setVisibility(View.GONE);
                        spinner.setVisibility(View.GONE);
                    }

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(final AdapterView<?> adapterView, View view, int i, long l) {
                            if (spinner.getSelectedItem().toString() == "New") {
                                final AlertDialog.Builder build = new AlertDialog.Builder(result.this);
                                build.setTitle("Enter new customer name");
                                final View custom = getLayoutInflater().inflate(R.layout.custom_alert, null);
                                build.setView(custom)
                                        .setCancelable(false);
                                build.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        customername = custom.findViewById(R.id.customername);

                                        if (TextUtils.isEmpty(customername.getText().toString())) {
                                            int pos = adaptar.getPosition("CUSTOMER");
                                            spinner.setSelection(pos);
                                        } else {
                                            list.add(customername.getText().toString());
                                            int pos = adaptar.getPosition(customername.getText().toString());
                                            spinner.setSelection(pos);

                                            // save in database
                                            customer_post cus = new customer_post(result.this);
                                            cus.execute(customername.getText().toString(),"0");
                                        }
                                    }
                                });
                                build.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        int pos = adaptar.getPosition("CUSTOMER");
                                        spinner.setSelection(pos);
                                    }
                                });
                                AlertDialog alertDialog = build.create();
                                alertDialog.show();
                            }
                            return;
                        }

                        public void onNothingSelected(AdapterView<?> adapterView) {
                            Toast.makeText(getApplicationContext(), "nothing", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    });
                }
            }
            else{
                scanerror();
            }
        }
        catch (NumberFormatException ex)
        {
            scanerror();
        }
    }

    // show the error if the qr is not valid
    public void scanerror()
    {
        AlertDialog.Builder build = new AlertDialog.Builder(result.this);
        build.setTitle("Scanned status");
        build.setMessage("Invalid Qrcode ")
                .setCancelable(false);
        build.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(intent);
                finish();
            }
        });
        AlertDialog alertDialog = build.create();
        alertDialog.show();
    }

    // submit data to database
    public void gotobackend()
    {
        // flag session for submit check
        sessionmanage sobj = new sessionmanage(this);
        if(sobj.getflagSession() == false) {
          //  String date = new SimpleDateFormat("dd-MM-yyyy|hh:mm:ss").format(Calendar.getInstance().getTime());
           if(finaldata[3].equalsIgnoreCase("stock"))
           {
               background obj = new background(this);
               if(spinner.getSelectedItem().toString().equalsIgnoreCase("CUSTOMER")){
                   obj.execute(finaldata[1],finaldata[2],finaldata[8],scandate.getText().toString(),"null");
               }
               else{
                   obj.execute(finaldata[1],finaldata[2],finaldata[8],scandate.getText().toString(),spinner.getSelectedItem().toString());
               }
           }
           else{
               background obj = new background(this);
               obj.execute(finaldata[1],finaldata[2],finaldata[8],scandate.getText().toString(),"null");
               // uid , boxno , db type , date,customer.
           }
        }
        else{
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Submit Status");
            build.setMessage("Already submitted");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
    }

    // submit button check network and redirect to gotodatabase
    public void checknetwork(View view)
    {
        checknetwork checknetwork = new checknetwork(this);
        if(checknetwork.checknetworkbool())
        {
            gotobackend();
        }
    }

    public void callcalender(View view)
    {
        datepicker datepicker = new datepicker();
        datepicker.show(getSupportFragmentManager(),"Date Picker");
    }
    // set calender
    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,i);
        calendar.set(Calendar.MONTH,i1);
        calendar.set(Calendar.DAY_OF_MONTH,i2);
        showDate(i, i1+1, i2);
    }
    private void showDate(int year, int month, int day) {
        m = ""+month;d = ""+day;
        if(month<10){
            m = "0"+month;
        }
        if (day<10){
            d = "0"+day;
        }
        StringBuilder date = new StringBuilder().append(year).append("-")
                .append(m).append("-").append(d);
        scandate.setText(date);
    }
}