package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class search_database extends AppCompatActivity {

    ViewPager viewpage;
    TabLayout tablayout;
    TabItem production,stock;
    Spinner spinner;
    ArrayAdapter adaptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_database);

        tablayout = findViewById(R.id.tab);
        viewpage = findViewById(R.id.viewpager);
        production = findViewById(R.id.production);
        stock = findViewById(R.id.stock);


        pageadaptar page = new pageadaptar(getSupportFragmentManager());

        viewpage.setAdapter(page);

        tablayout.setupWithViewPager(viewpage);

        /* spinner = stock.findViewById(R.id.spinner2);
        spinner.setVisibility(View.GONE);
        String[] size = {"3ply","4ply","6ply","8ply","9ply","12ply","16ply"};
        adaptar = new ArrayAdapter(this,android.R.layout.simple_spinner_item,size);
        adaptar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptar); */
/*
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpage.setCurrentItem((tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        }); */
    }
}