package com.applet.qr_code;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class dispatch_bags extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TextView dispatch_date,dispatch_tbags,dispatch_tnetwt,dispatch_tgrosswt;
    private String bags,m,d;
    EditText customername;
    ArrayAdapter adaptar;public static Spinner spinner;
    public static int tnetwt = 0,tbags = 0,tgrosswt = 0;
    public static List<String> customerlist = new ArrayList<String>();
    String datess = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_bags);

        bags = getIntent().getStringExtra("bags");
        dispatch_tbags = findViewById(R.id.dispatch_tbags);
        dispatch_tnetwt = findViewById(R.id.dispatch_tnetwt);
        dispatch_tgrosswt = findViewById(R.id.dispatch_tgrosswt);
        dispatch_tbags.setText(Integer.toString(tbags));
        dispatch_tnetwt.setText(Integer.toString(tnetwt));
        dispatch_tgrosswt.setText(Integer.toString(tgrosswt));
        TextView date = findViewById(R.id.dispatch_date);
        TextView sdate = findViewById(R.id.dispatch_sdate);
        date.setText(datess);
        sdate.setText(datess);

        customerlist.clear();
        customerlist.add("CUSTOMER");
        customerlist.add("New");

        customers cobj = new customers(this);
        cobj.execute();
        spinner = findViewById(R.id.spinner2);
        adaptar = new ArrayAdapter(this, android.R.layout.simple_spinner_item,customerlist);
        adaptar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptar);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner.getSelectedItem().toString() == "New") {
                    final AlertDialog.Builder build = new AlertDialog.Builder(dispatch_bags.this);
                    build.setTitle("Enter new customer name");
                    final View custom = getLayoutInflater().inflate(R.layout.custom_alert, null);
                    build.setView(custom)
                            .setCancelable(false);
                    build.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            customername = custom.findViewById(R.id.customername);

                            if (TextUtils.isEmpty(customername.getText().toString())) {
                                int pos = adaptar.getPosition("CUSTOMER");
                                spinner.setSelection(pos);
                            } else {
                                customerlist.add(customername.getText().toString());
                                int pos = adaptar.getPosition(customername.getText().toString());
                                spinner.setSelection(pos);

                                // save in database
                                customer_post cus = new customer_post(dispatch_bags.this);
                                cus.execute(customername.getText().toString(),"1");
                            }
                        }
                    });
                    build.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            int pos = adaptar.getPosition("CUSTOMER");
                            spinner.setSelection(pos);
                        }
                    });
                    AlertDialog alertDialog = build.create();
                    alertDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
    }

    public void callcalender(View view)
    {
        dispatch_date = findViewById(R.id.dispatch_date);
        datepicker datepicker = new datepicker();
        datepicker.show(getSupportFragmentManager(),"Date Picker");
    }
    public void callcalender2(View view)
    {
        dispatch_date = findViewById(R.id.dispatch_sdate);
        datepicker datepicker = new datepicker();
        datepicker.show(getSupportFragmentManager(),"Date Picker");
    }
    // set calender
    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,i);
        calendar.set(Calendar.MONTH,i1);
        calendar.set(Calendar.DAY_OF_MONTH,i2);
        showDate(i, i1+1, i2);
    }
    private void showDate(int year, int month, int day) {
        m = ""+month;d = ""+day;
        if(month<10){
            m = "0"+month;
        }
        if (day<10){
            d = "0"+day;
        }
        StringBuilder date = new StringBuilder().append(year).append("-")
                .append(m).append("-").append(d);
        dispatch_date.setText(date);
    }

    public void dispatch_todatabase(View v)
    {
        EditText no =  findViewById(R.id.dispatch_no);
        TextView date = findViewById(R.id.dispatch_date);
        TextView sdate = findViewById(R.id.dispatch_sdate);
        if(no.getText().toString().equalsIgnoreCase(""))
        {
            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Submit Status");
            build.setMessage("Dispatch number is empty.");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
        }
        else
        {
            checknetwork checknetwork = new checknetwork(this);
            if(checknetwork.checknetworkbool())
            {
                background obj = new background(this);
                obj.execute("", bags, "stock", sdate.getText().toString(),spinner.getSelectedItem().toString(), no.getText().toString(), date.getText().toString());
            }
        }
    }
}