package com.applet.qr_code;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link production#newInstance} factory method to
 * create an instance of this fragment.
 */
public class production extends Fragment {

    public static TextView prouid,prosize,proordnum,prodate,bremain,nremain,gremain,bused,nused,gused,purchasecustomer;
    public static TableLayout table;
    public static TableRow row;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Spinner spinner;
    ArrayAdapter adaptar;
    TextView search_pro_date;
    public production() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment production.
     */
    // TODO: Rename and change types and number of parameters
    public static production newInstance(String param1, String param2) {
        production fragment = new production();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_production, container, false);

        table = view.findViewById(R.id.table);
        prouid = view.findViewById(R.id.prouid);
        prosize = view.findViewById(R.id.prosize);
        proordnum = view.findViewById(R.id.proordnum);
        prodate = view.findViewById(R.id.prodate);
        purchasecustomer = view.findViewById(R.id.purchasecustomer);
        bremain = view.findViewById(R.id.rbags);
        nremain = view.findViewById(R.id.rnetwt);
        gremain = view.findViewById(R.id.rgrosswt);
        bused = view.findViewById(R.id.ubags);
        nused = view.findViewById(R.id.unetwt);
        gused = view.findViewById(R.id.ugrosswt);
        search_pro_date = view.findViewById(R.id.production_search_date);

        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        search_pro_date.setText(date);

        ImageView pro_search_date = view.findViewById(R.id.imageViewpro2);
        pro_search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calandercall();
            }
        });

        spinner = view.findViewById(R.id.spinner2);
        String[] size = {"All Size","210D","420D","630D","840D","1050D","1260D"};
        adaptar = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,size);
        adaptar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptar);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, View view, int i, long l) {

                int count = table.getChildCount();
                table.removeViews(1, count-1);

                currentproduction currentproduction = new currentproduction(getContext());
                currentproduction.execute(search_pro_date.getText().toString(),spinner.getSelectedItem().toString());

                return;
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getActivity().getApplicationContext(),"nothing",Toast.LENGTH_SHORT).show();
                return;
            }
        });

        TextView text = view.findViewById(R.id.allproduction);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allproduction();
            }
        });

          /*  row = new TableRow(getContext());
            row.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, 152));

            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Monospace.ttf");

            TextView sno = new TextView(getContext());
            sno.setText("4");
            sno.setTextSize(18);
            sno.setTextColor(Color.BLACK);
            sno.setHeight(150);
            sno.setWidth(70);
            sno.setTypeface(typeface);
            sno.setGravity(Gravity.CENTER);
            row.addView(sno);

            TextView scanndate = new TextView(getContext());
            scanndate.setText("12/12/2020");
            scanndate.setTextSize(18);
            scanndate.setTextColor(Color.BLACK);
            scanndate.setHeight(150);
            //label_hello.setWidth(300);
            scanndate.setTypeface(typeface);
            scanndate.setGravity(Gravity.CENTER);
            row.addView(scanndate);

            TextView label_android = new TextView(getContext());
            label_android.setText("Customer");
            label_android.setTextSize(18);
            label_android.setTextColor(Color.BLACK);
            label_android.setHeight(150);
            label_android.setTypeface(typeface);
            label_android.setGravity(Gravity.CENTER);
            row.addView(label_android);

            table.addView(row); */

       /* currentproduction currentproduction = new currentproduction(getContext());
        currentproduction.execute(spinner.getSelectedItem().toString()); */

        return view;
    }
    public void allproduction()
    {
        Intent intent = new Intent(getContext(),allproduction.class);
        startActivity(intent);
    }
    public void calandercall()
    {
        view_details.search_key = 0;
        datepicker datepicker = new datepicker();
        datepicker.show(getFragmentManager(),"Date Picker");
    }
}