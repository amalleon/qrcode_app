package com.applet.qr_code;

import android.app.DatePickerDialog;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import java.util.Calendar;

public class view_details extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    TextView search_date;
    Spinner spinner;
    TableLayout table;
    StringBuilder date;
    String m,d;
    public static int search_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);
        //SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        pageadaptar page = new pageadaptar(getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(page);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,i);
        calendar.set(Calendar.MONTH,i1);
        calendar.set(Calendar.DAY_OF_MONTH,i2);
        showDate(i, i1+1, i2);
    }
    private void showDate(int year, int month, int day) {

        m = ""+month;d = ""+day;
        if(month<10){
            m = "0"+month;
        }
        if (day<10){
            d = "0"+day;
        }
        date = new StringBuilder().append(year).append("-")
                .append(m).append("-").append(d);

        if(search_key == 0)
        {
            search_date = findViewById(R.id.production_search_date);
            search_date.setText(date);

            table = findViewById(R.id.table);
            int count = table.getChildCount();
            table.removeViews(1, count-1);

            spinner = findViewById(R.id.spinner2);
            currentproduction currentproduction = new currentproduction(this);
            currentproduction.execute(search_date.getText().toString(),spinner.getSelectedItem().toString());
        }
        else if(search_key == 1)
        {
            search_date = findViewById(R.id.stock_search_date);
            search_date.setText(date);

            table = findViewById(R.id.table2);
            int count = table.getChildCount();
            table.removeViews(1, count-1);

            spinner = findViewById(R.id.spinner3);
            currentstock currentstock = new currentstock(this);
            currentstock.execute(search_date.getText().toString(),spinner.getSelectedItem().toString());
        }
        else if(search_key == 2)
        {
            search_date = findViewById(R.id.stocklist_search_date);
            search_date.setText(date);
            table = findViewById(R.id.table2);
            int count = table.getChildCount();
            table.removeViews(1, count-1);

            currentstocklist currentstocklist = new currentstocklist(this);
            currentstocklist.execute(search_date.getText().toString());
        }
    }
}