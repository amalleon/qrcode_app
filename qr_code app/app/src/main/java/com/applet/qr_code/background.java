package com.applet.qr_code;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class background extends AsyncTask<String,Void,String> {

    ProgressDialog p;
    Button submit;
    Context context;
    String last = "Done",link,pdate,pno,unqid,type;
    AlertDialog alertDialog;
    background (Context ctx) {
        context = ctx;
    }
    @Override
    protected String doInBackground(String... params) {

        //String login_url = "http://192.168.43.64/revenge/HTML5Application4/public_html/qr_code/appphp/scann.php";
        urls urls = new urls();
        unqid = params[0];
        String boxno = params[1];
        type = params[2];
        String scndate = params[3];
        String customer = params[4];

        if (type.equalsIgnoreCase("stock") && unqid.equalsIgnoreCase("")) {
            pdate = params[6];
            pno = params[5];
            link = urls.stock_qr_submit();
        }
        else {
            pdate = "";
            pno = "";
            link = urls.qr_submit();
        }

        URL url = null;
        try {
            url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            String post_data = URLEncoder.encode("uniqueid","UTF-8")+"="+URLEncoder.encode(unqid,"UTF-8")+"&"
                    +URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode(type,"UTF-8")+"&"
                    +URLEncoder.encode("scanneddate","UTF-8")+"="+URLEncoder.encode(scndate,"UTF-8")+"&"
                    +URLEncoder.encode("customer","UTF-8")+"="+URLEncoder.encode(customer,"UTF-8")+"&"
                    +URLEncoder.encode("boxnumber","UTF-8")+"="+URLEncoder.encode(boxno,"UTF-8")+"&"
                    +URLEncoder.encode("pdate","UTF-8")+"="+URLEncoder.encode(pdate,"UTF-8")+"&"
                    +URLEncoder.encode("pno","UTF-8")+"="+URLEncoder.encode(pno,"UTF-8");
            bufferedWriter.write(post_data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            int response_code = httpURLConnection.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            }
            else{
                return "unsuccessful";
            }
        }
        catch (MalformedURLException ex) {
            ex.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
            return "exception";
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        p = new ProgressDialog(context);
        p.setMessage("Please wait...Its updating");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }

    @Override
    protected void onPostExecute(String result) {

        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful"))
        {
            AlertDialog.Builder build = new AlertDialog.Builder(context);
            build.setTitle("Submit Status");
            build.setMessage("Server not connected...");
            AlertDialog alertDialog = build.create();
            alertDialog.show();
            p.hide();
        }
        else if(result.equalsIgnoreCase("Successfully Submitted")){
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Submit Status");
            alertDialog.setMessage(result);
            alertDialog.show();
            p.hide();
            sessionmanage sobj = new sessionmanage(context);
            sobj.flagsession();
            if (type.equalsIgnoreCase("stock") && unqid.equalsIgnoreCase("")) {
                stock_qrsubmit.stock_items_id_list.clear();
                stock_qrsubmit.stock_items_list.clear();
            }
        }
        else{
            alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Submit Status");
            alertDialog.setMessage(result);
            alertDialog.show();
            p.hide();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        last += "On processing";
        super.onProgressUpdate(values);
    }
}
